import React from 'react'
import ReactDOM from 'react-dom/client'
import Body from './Body.jsx'
import './css/index.css'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    {/* <LottieControl /> */}
    <Body />
    {/* <Header />
    <Projects /> */}
  </React.StrictMode>,
)
