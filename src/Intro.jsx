import "./css/Intro.css";
import animation from "./assets/beau_graphic.json";
import Lottie from "lottie-react";

function Intro() {
  return (
    <>
      <div className="container-xl bg-dark my-5">
        <div className="row">
          <div className="col-sm-10 col-md-6  text-white ">
            <div className="p-3">
              <div
                className="border rounded p-2"
                style={{ backgroundColor: "rgb(97, 97, 97)" }}
              >
                <pre>
                  <code className="break-words text-md lg:text-lg">
                    <span>Hello friend 👋</span>
                  </code>
                </pre>
                <pre>
                  <code className="break-words text-md lg:text-lg">
                    <span>I'm Beau Bullitt</span>
                  </code>
                </pre>
                <pre>
                  <code className="break-words text-md lg:text-lg">
                    <span>Full Stack Software Developer</span>
                  </code>
                </pre>
              </div>
            </div>
            <div className="p-3">
              <div
                className="border rounded bg-gray p-2"
                style={{ backgroundColor: "rgb(97, 97, 97)" }}
              >
                <pre>
                  <code className="break-words text-md lg:text-lg">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-gitlab"
                      viewBox="0 0 16 16"
                    >
                      <path d="m15.734 6.1-.022-.058L13.534.358a.568.568 0 0 0-.563-.356.583.583 0 0 0-.328.122.582.582 0 0 0-.193.294l-1.47 4.499H5.025l-1.47-4.5A.572.572 0 0 0 2.47.358L.289 6.04l-.022.057A4.044 4.044 0 0 0 1.61 10.77l.007.006.02.014 3.318 2.485 1.64 1.242 1 .755a.673.673 0 0 0 .814 0l1-.755 1.64-1.242 3.338-2.5.009-.007a4.046 4.046 0 0 0 1.34-4.668Z" />
                    </svg>
                    <span className="text-wrap">
                      Gitlab: https://gitlab.com/beaubullitt
                    </span>
                  </code>
                </pre>
                <pre>
                  <code className="break-words text-md lg:text-lg">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-linkedin"
                      viewBox="0 0 16 16"
                    >
                      <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z" />
                    </svg>
                    <span className="text-wrap">
                      LinkedIn: https://www.linkedin.com/in/beau-bullitt/
                    </span>
                  </code>
                </pre>
                <pre>
                  <code className="break-words text-md lg:text-lg">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="16"
                      height="16"
                      fill="currentColor"
                      className="bi bi-file-earmark-person-fill"
                      viewBox="0 0 16 16"
                    >
                      <path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM11 8a3 3 0 1 1-6 0 3 3 0 0 1 6 0zm2 5.755V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1v-.245S4 12 8 12s5 1.755 5 1.755z" />
                    </svg>
                    <span className="text-wrap">
                      Resume: https://shorturl.at/acjqC{" "}
                    </span>
                  </code>
                </pre>
              </div>
            </div>
            <div className="">
              <ul className="justify-content-center">
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  JavaScript
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  Python
                </li>
                <li className="d-inline-block border  p-1 m-1  custom-skill">
                  React
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  FastAPI
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  HTML
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  CSS
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  AWS
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  Bootstrap
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  Django
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  MongoDB
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  PostgreSQL
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  WebSockets
                </li>
              </ul>
            </div>
          </div>
          <div
            className="col-sm-10 col-md-6"
            style={{ maxHeight: "500px", maxWidth: "100%" }}
            // style={{ maxWidth: "70%" }}
          >
            <Lottie animationData={animation} loop={true} />
          </div>
        </div>
      </div>
    </>
  );
}

export default Intro;
