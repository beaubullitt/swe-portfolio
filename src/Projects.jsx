import React from "react";
import "./assets/landing_page.png";

function Projects() {
  return (
    <>
      <div
        className="container-xl text-white py-5"
        id="projects"
        style={{ backgroundColor: "rgb(96, 96, 96)" }}
      >
        <div className="mb-5 pb-5">
          <h1>Projects</h1>
        </div>

        <div className="row justify-content-center">
          <div className="justify-content-center col-md-5 m-6 p-6 ">
            {/* Project Text Section */}
            <h1 className="">LangoTango</h1>

            <div className="">
              <ul className="justify-content-center p-2">
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  JavaScript
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  Python
                </li>
                <li className="d-inline-block border  p-1 m-1  custom-skill">
                  React
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  FastAPI
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  HTML
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  CSS
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  AWS
                </li>
                <li className="d-inline-block border  p-1 m-1 custom-skill">
                  MongoDB
                </li>
              </ul>
            </div>

            <div className="justify-content-center ">
              <div className="d-inline p-2">
                <button type="button" className="btn btn-outline-light">
                  <a
                    className="text-white"
                    href="https://languini-league.gitlab.io/languini-project/"
                  >
                    Live Website
                  </a>
                </button>
              </div>

              <div className="d-inline p-2 ">
                <button type="button" className="btn btn-outline-light">
                  <a
                    className="text-white"
                    href="https://gitlab.com/languini-league/languini-project"
                  >
                    Gitlab Repo
                  </a>
                </button>
              </div>
            </div>
            <p className="p-1 pt-3">
              LangoTango is a fullstack software engineering appliacation that
              utilizes AI and speech synthesis so that you have an on-demand
              language learning buddy. You can speak to the app in any available
              language and it will respond in said language. You can even prompt
              the app by saying things such as "answer me in 2 sentences or
              less" or "answer me as if you are speaking to a 5 year old." App
              uses Web Speech API and ChatGPT API's, React Frontend, FastAPI
              Backend, and AWS S3 Buckets.
            </p>
          </div>

          {/* Media Section */}
          <div className=" col-md-7">
          <div className="ratio ratio-16x9 p-3">
    <iframe
      // className="embed-responsive-item"
      src="https://www.youtube.com/embed/wndBrCL44mg?si=_2UDFMlN4wuNXGsc"
      title="LangoTangoYoutubeDemo"
      allowFullScreen
    ></iframe>

  </div>
            </div>
            {/* <div
              id="lango-tango-carousel"
              className="carousel slide"
              data-ride="carousel"
            >
              <button
                className="carousel-control-prev"
                href="#lango-tango-carousel"
                role="button"
                data-slide="prev"
              >
                <span
                  className="carousel-control-prev-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only"></span>
              </button>

              <button
                className="carousel-control-next"
                href="#lango-tango-carousel"
                role="button"
                data-slide="next"
              >
                <span
                  className="carousel-control-next-icon"
                  aria-hidden="true"
                ></span>
                <span className="sr-only"></span>
              </button>

              <div className="carousel-inner">
                <div className="carousel-item active">
                  <img
                    className="d-block w-100"
                    src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExM3M2dGQ1anI3MHA4aWt0NXV3ZGJ2NDA2MXc3aGd0ZHE0eHpkdWtxayZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/N9bXjXaWQcjBLGbQ0u/giphy.gif"
                    alt="First slide"
                  ></img>
                </div>

                <div className="carousel-item ">
                  <img
                    className="d-block w-100"
                    src="https://scontent-hou1-1.xx.fbcdn.net/v/t1.6435-9/175624405_3816243058424033_832796761850337624_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=be3454&_nc_ohc=sB6Ezs_TQfMAX9WX78U&_nc_ht=scontent-hou1-1.xx&oh=00_AfD91zBi7-1TRABlTlxrXXFW6_3zlU9olTMdqIj6sWCeqg&oe=654E5D41"
                    alt="Second slide"
                  ></img>
                </div> */}
            {/* </div>
            </div> */}
            {/* <div>
            <img
              src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExM3M2dGQ1anI3MHA4aWt0NXV3ZGJ2NDA2MXc3aGd0ZHE0eHpkdWtxayZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/N9bXjXaWQcjBLGbQ0u/giphy.gif"
              className="gif-fluid"
              style={{ maxWidth: "100%" }}
              alt="Lango Tango Functionality gif"
            />
            </div> */}
          </div>

      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    </>
  );
}

export default Projects;
