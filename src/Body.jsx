import Projects from "./Projects.jsx";
import Intro from "./Intro.jsx";
import Contact from "./Contact.jsx";
import Footer from "./Footer.jsx";
import Navbar from "./Navbar.jsx";
import "./css/body.css";

function Body() {
  return (
    <>
    <div className="bg-dark">
      <Navbar />
      <section>
        <Intro />
      </section>
      <section>
        <Projects />
      </section>
      <section>
        <Contact />
      </section>
      <section>
        <Footer />
      </section>
    </div>
    </>
  );
}
// ReactDOM.createRoot(document.getElementById('root')).render(
//     <React.StrictMode>
//       <Header />
//       <Projects />
//     </React.StrictMode>,
//   )
export default Body;
