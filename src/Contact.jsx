import animation from "./assets/goodbye.json";
import Lottie from "lottie-react";

function Contact() {
  return (
    <>
      <div className="container-xl bg-dark my-5"
      id="contact">
        <div className="row">
          <div className="mb-6 text-white col-sm-10 col-md-4 text-center align-center">
            <h3>Interested in working together? Let's connect!</h3>
            <p>Send me an email at: bullitt@berkeley.edu</p>
            <p>Check out my <a className="text-white"href="https://docs.google.com/document/d/1xaKCi78o5IymDBJI9k5X4iDG8xN0phtny9FSLiHq4qA/edit">resume</a></p>
          </div>
          <div className="col-sm-10 col-md-8 ">
            <Lottie animationData={animation} loop={true}/>
          </div>
        </div>
      </div>
    </>
  );
}

export default Contact;
